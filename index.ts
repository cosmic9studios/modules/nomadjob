

import * as pulumi from "@pulumi/pulumi";
import axios, { AxiosRequestConfig, AxiosPromise, AxiosResponse } from 'axios'

interface NomadJobInputs {
    address: string, 
    hclJob: string,
    vars: Object
};

const nomadJobProvider: pulumi.dynamic.ResourceProvider = {
    async create(inputs: NomadJobInputs) {
        return { 
            id: await runJob(inputs),
            outs: {}
        };
    },
}

export default class NomadJob extends pulumi.dynamic.Resource {
    constructor(name: string, args: NomadJobInputs, opts?: pulumi.CustomResourceOptions) {
        super(nomadJobProvider, name, args, opts);
    }
}

function makeTemplate(templateString: string, templateVariables: object) {
	const keys = Object.keys(templateVariables);
	const values = Object.values(templateVariables);
	let templateFunction = new Function(...keys, `return \`${templateString}\`;`);
	return templateFunction(...values);
}

async function runJob(inputs: NomadJobInputs) {
    const hclJob = makeTemplate(inputs.hclJob, inputs.vars);
    const address = inputs.address;

    try {
        const parseResponse = await axios.post(`${address}/v1/jobs/parse`, {JobHCL: hclJob});
        await axios.post(`${address}/v1/jobs`, { Job: parseResponse.data });

        return parseResponse.data.job.id;

    } catch (err) {
        return err;
    }
}